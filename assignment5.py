#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

# Dictionary of books
books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    # <your code>
    return jsonify(books)

@app.route('/')
@app.route('/book/')
def showBook():
    # <your code>
    
    return render_template('showBook.html', books = books)
    
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    # <your code>
    if request.method == 'POST':
        title = request.form['title']

        # Determine the new ID
        new_id = 1  # Default to 1 if books is empty
        if books:
            # Find the maximum id in existing books
            max_id = max(int(book['id']) for book in books)
            new_id = max_id + 1
        
        # Create the new book dictionary
        new_book = {'title': title, 'id': str(new_id)}
        
        # Append new_book to the books list
        books.append(new_book)
        
        # Redirect to the showBook route
        return redirect(url_for('showBook'))
    
    # Render the newBook.html template for GET requests
    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    # <your code>
    if request.method == 'GET':
        # Find the book with the given book_id from the books list
        for book in books:
            if book['id'] == str(book_id):
                return render_template('editBook.html', book=book)
        # If book_id is not found, you might want to handle this case
        return 'Book not found', 404
    elif request.method == 'POST':
        # Update the book in the books list based on form submission
        for book in books:
            if book['id'] == str(book_id):
                book['title'] = request.form['title']  # Assuming you have a form field named 'title'
                # Update other fields as needed
                return redirect(url_for('showBook'))  # Redirect to the books list page after editing
        # If book_id is not found, you might want to handle this case
        return 'Book not found', 404

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    # <your code>
    if request.method == 'GET':
        # Find the book with the given book_id from the books list
        for book in books:
            if book['id'] == str(book_id):
                return render_template('deleteBook.html', book=book)
        # If book_id is not found, you might want to handle this case
        return 'Book not found', 404
    elif request.method == 'POST':
        # Update the book in the books list based on form submission
        for book in books:
            if book['id'] == str(book_id):
                books.remove(book)
                # Update other fields as needed
                return redirect(url_for('showBook'))  # Redirect to the books list page after editing
        # If book_id is not found, you might want to handle this case
        return render_template('showBook.html', book=book)
    return render_template('deleteBook.html')
if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)